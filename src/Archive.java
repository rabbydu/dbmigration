import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import databasemanager.DatabaseManager;

public class Archive {
	public void dropTable(String tableName) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("drop table IF EXISTS  " + tableName + " ;");
			System.out.println("dropped table " + tableName);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createTable(String tableName) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE  IF EXISTS " + tableName);

			stmt.executeUpdate("Create table " + tableName + " LIKE vbvisitor;");

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("created new table " + tableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createTable(String tableName, String sourceTable) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();

			stmt.executeUpdate("DROP TABLE  IF EXISTS " + tableName);
			stmt.executeUpdate("Create table " + tableName + " LIKE " + sourceTable + ";");

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("created new table " + tableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void alterTableEngine(String tableName, String engineName) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("ALTER TABLE " + tableName + "  engine = '" + engineName + "';");
			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("altered table " + tableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bulkInsert(String destination, String source) {
		String query = "INSERT INTO " + destination
				+ " ( ID, vsVisitorID, vsOnlineStatus,vsFirstVisitTime,vsLastLoginTIme,vsLastLogoutTime,vsAccount) " + "SELECT"
				+ " ID, vsVisitorID, vsOnlineStatus,vsFirstVisitTime,vsLastLoginTIme,vsLastLogoutTime,vsAccount FROM " + source;

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bulkInsertVbVsitiorRecord(String destination, String source) {
		String query = "INSERT INTO " + destination + " (\n" + "	ID,\n" + "	vsVisitorID,\n" + "	vsSessionID,\n"
				+ "	vsVisitorIP,\n" + "	vsVisitorPlatform,\n" + "	vsVisitorBrowserName,\n" + "	vsVisitorBrowserVersion,\n"
				+ "	vsVisitorCookies,\n" + "	vsVisitorUserAgent,\n" + "	vsVisitorCountry,\n" + "	vsVisitorRegion,\n"
				+ "	vsVisitorLattitude,\n" + "	vsVisitorLongitude,\n" + "	isp,\n" + "	vsVisitorCity,\n"
				+ "	vsVisitorReferrel,\n" + "	vsVisitCount,\n" + "	vsViewsCount,\n" + "	vsUpdateTime,\n"
				+ "	vsVisitorEmail,\n" + "	vsVisitorPhone,\n" + "	vsVisitorName,\n" + "	vsAccount\n" + "	)\n"
				+ "	SELECT \n" + "		ID,\n" + "	vsVisitorID,\n" + "	vsSessionID,\n" + "	vsVisitorIP,\n"
				+ "	vsVisitorPlatform,\n" + "	vsVisitorBrowserName,\n" + "	vsVisitorBrowserVersion,\n"
				+ "	vsVisitorCookies,\n" + "	vsVisitorUserAgent,\n" + "	vsVisitorCountry,\n" + "	vsVisitorRegion,\n"
				+ "	vsVisitorLattitude,\n" + "	vsVisitorLongitude,\n" + "	isp,\n" + "	vsVisitorCity,\n"
				+ "	vsVisitorReferrel,\n" + "	vsVisitCount,\n" + "	vsViewsCount,\n" + "	vsUpdateTime,\n"
				+ "	vsVisitorEmail,\n" + "	vsVisitorPhone,\n" + "	vsVisitorName,\n" + "	vsAccount\n" + "	FROM " + source
				+ ";";

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertIntoTemp(String tempTableName) {
		String query = "INSERT INTO " + tempTableName
				+ "(ID, vsVisitorID, vsOnlineStatus, vsFirstVisitTime, vsLastLoginTIme, vsLastLogoutTime, vsAccount)"
				+ " SELECT vbvisitor.ID, vbvisitor.vsVisitorID, vsOnlineStatus, vsFirstVisitTime, vsLastLoginTIme, vsLastLogoutTime, vsAccount "
				+ " FROM " + " vbvisitor " + "INNER JOIN ( " + "	SELECT DISTINCT vsVisitorID FROM vbvisitorrecord "

				+ " INNER JOIN ( SELECT vbmissedchats.vsRecordID AS RID FROM vbmissedchats " + " UNION "
				+ " SELECT vbofflinemessage.vbVisitorRID AS RID FROM vbofflinemessage " + ") t ON t.RID = vbvisitorrecord.ID "

				+ " ) tt ON vbvisitor.ID = tt.vsVisitorID";

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);
			System.out.println("inserted into " + tempTableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertIntoVbVisitorRecordTemp(String tempTableName) {
		String query = "INSERT INTO " + tempTableName + " SELECT\n" + "	VR2.ID,\n" + "	VR2.vsVisitorID,\n"
				+ "	VR2.vsSessionID,\n" + "	VR2.vsVisitorIP,\n" + "	VR2.vsVisitorPlatform,\n" + "	VR2.vsVisitorBrowserName,\n"
				+ "	VR2.vsVisitorBrowserVersion,\n" + "	VR2.vsVisitorCookies,\n" + "	VR2.vsVisitorUserAgent,\n"
				+ "	VR2.vsVisitorCountry,\n" + "	VR2.vsVisitorRegion,\n" + "	VR2.vsVisitorLattitude,\n"
				+ "	VR2.vsVisitorLongitude,\n" + "	VR2.isp,\n" + "	VR2.vsVisitorCity,\n" + "	VR2.vsVisitorReferrel,\n"
				+ "	VR2.vsVisitCount,\n" + "	VR2.vsViewsCount,\n" + "	VR2.vsUpdateTime,\n" + "	VR2.vsVisitorEmail,\n"
				+ "	VR2.vsVisitorPhone,\n" + "	VR2.vsVisitorName,\n" + "	VR2.vsAccount\n" + " FROM\n"
				+ "	vbvisitorrecord VR2\n" + "INNER JOIN (\n" + "	SELECT\n" + "		ID,\n" + "		vsVisitorID\n"
				+ "	FROM\n" + "		vbvisitorrecord\n" + "	INNER JOIN (\n" + "		SELECT\n"
				+ "			vbmissedchats.vsRecordID AS RID\n" + "		FROM\n" + "			vbmissedchats\n" + "		UNION\n"
				+ "			SELECT\n" + "				vbofflinemessage.vbVisitorRID AS RID\n" + "			FROM\n"
				+ "				vbofflinemessage\n" + "	) t ON t.RID = vbvisitorrecord.ID\n"
				+ ") tt ON VR2.vsVisitorID = tt.vsVisitorID\n" + "GROUP BY\n" + "	VR2.ID;";

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);
			System.out.println("inserted into " + tempTableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void truncateTable(String newTableName) {

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DELETE FROM " + newTableName);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("Data Deleted from " + newTableName);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void initDbManager() {
		try {
			DatabaseManager.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void DropIndexFromTableName(String tableName) {
		Connection connection = null;
		Set<String> indexSet = new HashSet<String>();
		Statement st = null;
		Statement stDropIndex = null;
		ResultSet rs = null;
		try {
			connection = DatabaseManager.getInstance().getConnection();
			System.out.println("Started : Dropping Indexes from " + tableName + " @ " + Calendar.getInstance().getTime());
			String query = "SHOW INDEX FROM " + tableName;
			String dropIndexQuery = "ALTER TABLE " + tableName + " DROP INDEX ";
			stDropIndex = connection.createStatement();
			rs = null;
			st = connection.createStatement();
			rs = st.executeQuery(query);

			int count = 0;
			while (rs.next()) {
				String keyName = rs.getString("Key_name");
				if (keyName.equalsIgnoreCase("PRIMARY") == false) {
					boolean flag = indexSet.add(keyName);
					if (flag)
						count++;
					System.out.println("Detected " + count + " index");
				}
			}
			for (String index : indexSet) {
				System.out.println(count + " indexes remaining...");
				count--;
				String sql = dropIndexQuery + index;
				stDropIndex.executeUpdate(sql);
			}
			System.out.println("Finished : Dropping Indexes from " + tableName + " @ " + Calendar.getInstance().getTime());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (stDropIndex != null) {
				try {
					stDropIndex.close();
				} catch (SQLException ex) {
				}
			}
			if (st != null) {
				try {
					st.close();
				} catch (SQLException ex) {

				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {

				}
			}
		}
	}
	
//	private static void FilterVbSessionTable() {
//		try {
//			final DBMigrator db = new DBMigrator();
//			db.initDbManager();
//			db.DropIndexFromTableName("vbSession");
//			db.alterTableEngine("vbSession", "MyISAM");
//			db.truncateVbSessionTable();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}

	private void truncateVbSessionTable() {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DELETE FROM vbsession where userType=1;");

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("Data Deleted from vbSession Table");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
//	private static void FilterVbVisitorRecordTable() {
//		try {
//			final DBMigrator db = new DBMigrator();
//			final String backupTableName = "vbvisitorrecordBackup";
//			final String oldTableName = "vbvisitorrecord";
//			final String tempTalbeName = "vbvisitorrecordTemp";
//
//			db.initDbManager();
//
//			db.dropTable(backupTableName);
//
//			db.DropIndexFromTableName(oldTableName);
//
//			db.alterTableEngine(oldTableName, "MyISAM");
//
//			db.createTable(backupTableName, oldTableName);
//
//			db.bulkInsertVbVsitiorRecord(backupTableName, oldTableName);
//
//			db.createTable(tempTalbeName, oldTableName);
//
//			db.insertIntoVbVisitorRecordTemp(tempTalbeName);
//
//			db.truncateTable(oldTableName);
//
//			db.bulkInsertVbVsitiorRecord(oldTableName, tempTalbeName);
//
//			db.dropTable(tempTalbeName);
//		} catch (Exception e) {
//
//		}
//
//	}
//
//	private static void FilterVbVisitorTable() {
//		try {
//			final DBMigrator db = new DBMigrator();
//			final String backupTableName = "vbvisitorBackup";
//			final String oldTableName = "vbvisitor";
//			final String tempTalbeName = "vbvisitorTemp";
//
//			db.initDbManager();
//
//			db.dropTable(backupTableName);
//
//			db.DropIndexFromTableName(oldTableName);
//
//			db.alterTableEngine(oldTableName, "MyISAM");
//
//			db.createTable(backupTableName);
//
//			db.bulkInsert(backupTableName, oldTableName);
//
//			db.createTable(tempTalbeName);
//
//			db.insertIntoTemp(tempTalbeName);
//
//			db.truncateTable(oldTableName);
//
//			db.bulkInsert(oldTableName, tempTalbeName);
//
//			db.dropTable(tempTalbeName);
//		} catch (Exception e) {
//
//		}
//
//	}
	
//	public static void main(String args[]) {
//	long start, end;
//	long totalTime = 0;
//	long diff = 0;
//	end = start = System.currentTimeMillis();
//	try {
//		VisitorAnalyticsScript.populateVbVisitorAnalytics();
//	} catch (Exception e) {
//
//	}
//	end = System.currentTimeMillis();
//	diff = end - start;
//	System.out.println("VisitorAnalytics Script took " + diff + " ms");
//	totalTime += diff;
//
//	start = System.currentTimeMillis();
//	try {
//		VisitorReferrerScript.populateVbVisitorReferrerTable();
//	} catch (Exception e) {
//
//	}
//	end = System.currentTimeMillis();
//
//	diff = end - start;
//	totalTime += diff;
//	System.out.println("Visitor Referrer Script took " + diff + " ms");
//
//	FilterVbVisitorTable();
//	end = System.currentTimeMillis();
//	diff = end - start;
//	totalTime += diff;
//	System.out.println("Filterring vbvisitor took " + diff + " ms");
//
//	start = System.currentTimeMillis();
//	FilterVbVisitorRecordTable();
//	end = System.currentTimeMillis();
//	diff = end - start;
//	totalTime += diff;
//	System.out.println("Filterring vbvisitorrecord took " + diff + " ms");
//
//	start = System.currentTimeMillis();
//	FilterVbSessionTable();
//	end = System.currentTimeMillis();
//	diff = end - start;
//	totalTime += diff;
//	System.out.println("Filterring vbsession took " + diff + " ms");
//
//	System.out.println("TOTAL TIME TOOK " + totalTime + " milliseconds");
//	System.exit(0);
//}
}
