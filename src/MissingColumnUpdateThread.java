import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import databasemanager.DatabaseManager;

public class MissingColumnUpdateThread implements Runnable {

	String tableName;
	ArrayList<MissedChatMissingColumnDTO> list;
	long endPoint;

	public MissingColumnUpdateThread(String tableName, ArrayList<MissedChatMissingColumnDTO> list, long endPoint) {
		this.tableName = tableName;
		this.list = list;
		this.endPoint = endPoint;
	}

	@Override
	public void run() {

		//System.out.println("Thread started for update from " + (endPoint - list.size()) + " to " + endPoint);

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			PreparedStatement ps;

			String query = "UPDATE " + tableName
					+ " SET chatStatus = ?, dateTimeStamp = ?, chatRequestTime = ?, isMissed = ?, chatRequestType = ?, "
					+ " channelType = ?, departmentId = ?, isTriggered = ?, isHumanHandover = ? WHERE vsChatInfoId = ?;";

			ps = connection.prepareStatement(query);

			int batchCount = 0;

			for (MissedChatMissingColumnDTO dto : list) {

				ps.setInt(1, dto.getChatStatus());
				ps.setLong(2, dto.getDateTimeStamp());
				ps.setLong(3, dto.getChatRequestTime());
				ps.setString(4, dto.getIsMissed());
				ps.setString(5, dto.getChatRequestType());
				ps.setInt(6, dto.getChannelType());
				ps.setLong(7, dto.getDepartmentId());
				ps.setString(8, dto.getIsTriggered());
				ps.setString(9, dto.getIsHumanHandover());
				ps.setLong(10, dto.getId());

				ps.addBatch();

				batchCount++;

				if (batchCount % VbVisitorServingScript.BATCH_UPDATE_SIZE == 0 || batchCount == list.size()) {
					//System.out.println("updating " + batchCount + " rows... :) ");
					ps.executeBatch();
					//System.out.println("updated " + batchCount + " rows... :) ");
				}
			}
			
			ps.close();

			VbVisitorServingScript.TOTAL_UPDATE--;
			System.out.println("Thread Completed: Total Update: " + VbVisitorServingScript.TOTAL_UPDATE);

			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		//System.out.println("Thread Completed for update from " + (endPoint - list.size()) + " to " + endPoint);

	}

}
