/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import databasemanager.DatabaseManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author subrata.pappu
 */
class VisitorReferrerDTO {

    String operatorCode;
    long google;
    long facebook;
    long direct;
    long other;
}

public class VisitorReferrerScript {

    static Map<String, VisitorReferrerDTO> map = new HashMap<String, VisitorReferrerDTO>();

    public static void populateVbVisitorReferrerTable() {
        String selectFromVbVisitorRecord = "SELECT \n"
                + "vsAccount,\n"
                + "CONVERT( IFNULL(SUM(CASE WHEN vsVisitorReferrel LIKE '%facebook%'  THEN 1 ELSE 0 END),'0') , UNSIGNED INTEGER) AS facebook,\n"
                + "CONVERT(IFNULL(SUM(CASE WHEN vsVisitorReferrel LIKE '%google%'  THEN 1 ELSE 0 END),'0') ,UNSIGNED INTEGER) AS google,\n"
                + "CONVERT(IFNULL(SUM(CASE WHEN vsVisitorReferrel LIKE 'direct' THEN 1 ELSE 0 END),'0') ,UNSIGNED INTEGER)  AS direct,\n"
                + "COUNT(*) AS total \n"
                + "FROM vbvisitorrecord\n"
                + "GROUP BY vsAccount";
        String insertVbVisitorReferrer = "INSERT INTO vbVisitorReferrer(operator_code,google,facebook,direct,other) "
                + "VALUES(?,?,?,?,?)";

        Connection connection = null;
        Statement stVbVsiitorRecord = null;
        ResultSet rsVbVisitorRecord = null;

        PreparedStatement psVbVisitorReferrer = null;

        try {
            connection = DatabaseManager.getInstance().getConnection();
            stVbVsiitorRecord = connection.createStatement();
            rsVbVisitorRecord = stVbVsiitorRecord.executeQuery(selectFromVbVisitorRecord);
            psVbVisitorReferrer = connection.prepareStatement(insertVbVisitorReferrer);

            while (rsVbVisitorRecord.next()) {
                String operatorCode = rsVbVisitorRecord.getString("vsAccount");
                VisitorReferrerDTO dto = new VisitorReferrerDTO();
                dto.operatorCode = operatorCode;
                dto.google = rsVbVisitorRecord.getLong("google");
                dto.facebook = rsVbVisitorRecord.getLong("facebook");
                dto.direct = rsVbVisitorRecord.getLong("direct");
                long total = rsVbVisitorRecord.getLong("total");
                long other = total - (dto.google + dto.facebook + dto.direct);

                dto.other = other;

                map.put(operatorCode, dto);
            }

            int count = 1;
            for (Map.Entry<String, VisitorReferrerDTO> entry : map.entrySet()) {
                try{
                	VisitorReferrerDTO dto = entry.getValue();
                    psVbVisitorReferrer.setString(1, dto.operatorCode);
                    psVbVisitorReferrer.setLong(2, dto.google);
                    psVbVisitorReferrer.setLong(3, dto.facebook);
                    psVbVisitorReferrer.setLong(4, dto.direct);
                    psVbVisitorReferrer.setLong(5, dto.other);
                    
                    psVbVisitorReferrer.executeUpdate();
                    
                }catch(Exception e){
                	
                }
                
                //System.out.println(count + "# " + entry.getKey() + " : " + dto.google + " : " + dto.direct);
                count++;
            }
            

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(VisitorReferrerScript.class.getName()).log(Level.SEVERE, null, ex);
        }  finally {

            if (rsVbVisitorRecord != null) {
                try {
                    rsVbVisitorRecord.close();
                } catch (Exception e) {

                }
            }

            if (stVbVsiitorRecord != null) {
                try {
                    stVbVsiitorRecord.close();
                } catch (Exception e) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {

                }
            }

        }
    }
}
