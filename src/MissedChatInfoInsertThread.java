import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import databasemanager.DatabaseManager;

public class MissedChatInfoInsertThread implements Runnable{
	
	String tableName;
	ArrayList<MissedChatMissingColumnDTO> list;
	long endPoint;

	public MissedChatInfoInsertThread(String tableName, ArrayList<MissedChatMissingColumnDTO> list, long endPoint) {
		this.tableName = tableName;
		this.list = list;
		this.endPoint = endPoint;
	}

	@Override
	public void run() {
		
		//System.out.println("Thread started for update from " + (endPoint - list.size()) + " to " + endPoint);
		
		try {
			
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			PreparedStatement ps;
			
			String query = "INSERT INTO " + tableName
					+ "(vsAgentID, vsVisitorID, vsAccount, vsConversationStartTime, vsConversationEndTime, vsVisitorRID, vsLastMsgID, vsChatEndedBy, "
					+ " vsChatInfoId, isTransferred, "
					+ " chatStatus, dateTimeStamp, chatRequestTime, isMissed, chatRequestType, channelType, departmentId, isTriggered, isHumanHandover) "
					+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			ps = connection.prepareStatement(query);
			
			int batchCount = 0;
			for (MissedChatMissingColumnDTO dto : list) {
				ps.setLong(1, dto.getAssignedAgentId());
				ps.setLong(2, dto.getAssignedVisitorId());
				ps.setString(3, dto.getVbAccount());
				ps.setLong(4, dto.getChatResponseTime());
				ps.setLong(5, 0);
				ps.setLong(6, dto.getVsRecordId());
				ps.setLong(7, -1);
				ps.setInt(8, -1);
				ps.setLong(9, dto.getId());
				ps.setInt(10, dto.getIsTransferred());

				ps.setInt(11, dto.getChatStatus());
				ps.setLong(12, dto.getDateTimeStamp());
				ps.setLong(13, dto.getChatRequestTime());
				ps.setString(14, dto.getIsMissed());
				ps.setString(15, dto.getChatRequestType());
				ps.setInt(16, dto.getChannelType());
				ps.setLong(17, dto.getDepartmentId());
				ps.setString(18, dto.getIsTriggered());
				ps.setString(19, dto.getIsHumanHandover());

				ps.addBatch();

				batchCount++;

				if (batchCount % VbVisitorServingScript.BATCH_UPDATE_SIZE == 0 || batchCount == list.size()) {
					ps.executeBatch();
				}
			}

			ps.close();

			DatabaseManager.getInstance().freeConnection(connection);
			
			VbVisitorServingScript.TOTAL_UPDATE--;
			System.out.println("Total Update: " + VbVisitorServingScript.TOTAL_UPDATE);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println("Thread Completed for update from " + (endPoint - list.size()) + " to " + endPoint);
		
	}

}
