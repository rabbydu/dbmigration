import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Scanner;

public class DBMigrator {
	
	public static DBMigrator main = new DBMigrator();
	private static String PROPERTY_FILE = "last_ids.properties";
	
	public void storeIntoProperties(String key, String value) {
		try {
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(PROPERTY_FILE);
			prop.load(in);
			prop.setProperty(key, value);
			prop.store(new FileOutputStream(PROPERTY_FILE), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		System.out.println("Program start");

		main.FilterVbVisitorServingTable();

	}

	/**
	 * vbvisitorserving table migration
	 */
	private void FilterVbVisitorServingTable() {
		try {

			System.out.println("migration started......");

			final VbVisitorServingScript db = new VbVisitorServingScript();
			final String backupTableName = "vbvisitorserving_bkp";
			final String oldTableName = "vbvisitorserving";
			final String tempTableName = "vbvisitorserving_temp";

			long startTime = System.currentTimeMillis();

			long st;
			
			
			long lastIdOfMissedChatTable = 0, lastIdOfVisitorServingTable = 0, lastIdOfVbratingTable = 0;
			
			lastIdOfMissedChatTable = db.lastID("vbmissedchats");
			main.storeIntoProperties("last_id_vbmissedchats", "" + lastIdOfMissedChatTable);
			lastIdOfVisitorServingTable = db.lastID(oldTableName);
			main.storeIntoProperties("last_id_vbvisitorserving", "" + lastIdOfVisitorServingTable);
			lastIdOfVbratingTable = db.lastID("vbrating");
			main.storeIntoProperties("last_id_vbrating", "" + lastIdOfVbratingTable);

			// 1. create backup table and insert
//			st = System.currentTimeMillis();
//			db.dropTable(backupTableName);
//			db.createTable(backupTableName, oldTableName);
//			db.alterTableEngine(backupTableName, "MyISAM");
//			db.bulkInsert(backupTableName, oldTableName);
//			db.alterTableEngine(backupTableName, "InnoDB");
//			System.out.println("=========== backup table time: " + (System.currentTimeMillis() - st));

			// 2. create temp table and insert
			db.dropTable(tempTableName);
			db.createTable(tempTableName, oldTableName);

			// 3. adding additional column
			db.addAdditionalColumn(tempTableName);

			// Change to engine to MyISAM for bulk insert
//			st = System.currentTimeMillis();
//			db.alterTableEngine(tempTableName, "MyISAM");
//			System.out.println("========== temp table engine change time: " + (System.currentTimeMillis() -st));

			st = System.currentTimeMillis();
			db.bulkInsert(tempTableName, oldTableName, lastIdOfVisitorServingTable);
			System.out.println("========== temp table bulk insert time: " + (System.currentTimeMillis() - st));

//			st = System.currentTimeMillis();
//			db.alterTableEngine(tempTableName, "InnoDB");
//			System.out.println("========== temp table engine change time: " + (System.currentTimeMillis() -st));
			
//			// 5. Fill new column data
			st = System.currentTimeMillis();
			db.fillupMissedChatMissingColumn(tempTableName, lastIdOfMissedChatTable);
			System.out.println("========== temp table missed chat missing column update time: " + (System.currentTimeMillis() - st));

			// 4. insert missed chat data where isMissed = 1
			st = System.currentTimeMillis();
			db.insertMissedChatInformation(tempTableName, lastIdOfMissedChatTable);
			System.out.println("========== temp table insert missed chat info time: " + (System.currentTimeMillis() - st));
//
//			// 6. insert rating information
			st = System.currentTimeMillis();
			db.insertRatingInformation(tempTableName, lastIdOfVbratingTable);
			System.out.println("========== temp table rating info insert time: " + (System.currentTimeMillis() - st));
//
//			st = System.currentTimeMillis();
			db.addAditionalIndexForServingTable(tempTableName);
			System.out.println("========== temp table additional index adding time: " + (System.currentTimeMillis() - st));
//
//			// 7. update vbchattransfer table
			st = System.currentTimeMillis();
			db.updateChatTransferTable(tempTableName);
			System.out.println("========== update chatTransfer table time: " + (System.currentTimeMillis() - st));
//
//			// 8. update vbsuccessfulchatrequest table
			st = System.currentTimeMillis();
			db.updateSuccessfullChatRequestTable(tempTableName);
			System.out.println("========== update successfullChatRequestTime time: " + (System.currentTimeMillis() - st));
//
//			// 9. update vbstoretaginfo table
			st = System.currentTimeMillis();
			db.updateStoreTagInfoTable(tempTableName);
			System.out.println("========== update storeTagInfo table time: " + (System.currentTimeMillis() - st));

			// 9. drop oldTableName
			// 10. rename tempTable to old table

			long endTime = System.currentTimeMillis();
			long executionTime = endTime - startTime;

			System.out.println("\n======== Execution Time: " + executionTime + " ===========\n");

			System.out.println("migration finished.......");

			System.exit(0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
