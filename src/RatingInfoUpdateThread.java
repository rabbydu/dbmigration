import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import databasemanager.DatabaseManager;

public class RatingInfoUpdateThread implements Runnable {

	String tableName;
	ArrayList<RatingDTO> list;
	long endPoint;

	public RatingInfoUpdateThread(String tableName, ArrayList<RatingDTO> list, long endPoint) {
		this.tableName = tableName;
		this.list = list;
		this.endPoint = endPoint;
	}

	@Override
	public void run() {
		
		//System.out.println("Thread started for update from " + (endPoint - list.size()) + " to " + endPoint);

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			PreparedStatement ps;

			String query = "UPDATE " + tableName + " SET rateType = ?, rateValue = ? WHERE ID = ?;";
			
			ps = connection.prepareStatement(query);
			
			int batchCount = 0;
			
			for (RatingDTO dto : list) {
				ps.setString(1, dto.getRateType());
				ps.setString(2, dto.getRateValue());
				ps.setLong(3, dto.getVisitorServingID());

				ps.addBatch();

				batchCount++;

				if (batchCount % VbVisitorServingScript.BATCH_UPDATE_SIZE == 0 || batchCount == list.size()) {
					//System.out.println("updating " + batchCount + " rows... :) ");
					ps.executeBatch();
					//System.out.println("updated " + batchCount + " rows... :) ");
				}
			}

			ps.close();
			
			VbVisitorServingScript.TOTAL_UPDATE--;
			System.out.println("Total Update: " + VbVisitorServingScript.TOTAL_UPDATE);

			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//System.out.println("Thread Completed for update from " + (endPoint - list.size()) + " to " + endPoint);

	}

}
