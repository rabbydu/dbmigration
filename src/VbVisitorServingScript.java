import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import databasemanager.DatabaseManager;

class MissedIdTOServingIdDTO {
	private long missedChatId;
	private long servingId;

	public long getMissedChatId() {
		return missedChatId;
	}

	public void setMissedChatId(long missedChatId) {
		this.missedChatId = missedChatId;
	}

	public long getServingId() {
		return servingId;
	}

	public void setServingId(long servingId) {
		this.servingId = servingId;
	}

}

class RatingDTO {

	private Long visitorServingID;
	private String rateType;
	private String rateValue;

	public Long getVisitorServingID() {
		return visitorServingID;
	}

	public void setVisitorServingID(Long visitorServingID) {
		this.visitorServingID = visitorServingID;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getRateValue() {
		return rateValue;
	}

	public void setRateValue(String rateValue) {
		this.rateValue = rateValue;
	}

}

class MissedChatMissingColumnDTO {

	private long id;
	private String vbAccount;
	private long assignedAgentId;
	private long assignedVisitorId;
	private long vsRecordId;
	private int chatStatus;
	private long dateTimeStamp;
	private long chatRequestTime;
	private long chatResponseTime;
	private String isMissed;
	private String chatRequestType;
	private int isTransferred;
	private int channelType;
	private long departmentId;
	private String isTriggered;
	private String isHumanHandover;

	public String getVbAccount() {
		return vbAccount;
	}

	public void setVbAccount(String vbAccount) {
		this.vbAccount = vbAccount;
	}

	public long getAssignedAgentId() {
		return assignedAgentId;
	}

	public void setAssignedAgentId(long assignedAgentId) {
		this.assignedAgentId = assignedAgentId;
	}

	public long getAssignedVisitorId() {
		return assignedVisitorId;
	}

	public void setAssignedVisitorId(long assignedVisitorId) {
		this.assignedVisitorId = assignedVisitorId;
	}

	public long getVsRecordId() {
		return vsRecordId;
	}

	public void setVsRecordId(long vsRecordId) {
		this.vsRecordId = vsRecordId;
	}

	public long getChatResponseTime() {
		return chatResponseTime;
	}

	public void setChatResponseTime(long chatResponseTime) {
		this.chatResponseTime = chatResponseTime;
	}

	public int getIsTransferred() {
		return isTransferred;
	}

	public void setIsTransferred(int isTransferred) {
		this.isTransferred = isTransferred;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getChatStatus() {
		return chatStatus;
	}

	public void setChatStatus(int chatStatus) {
		this.chatStatus = chatStatus;
	}

	public long getDateTimeStamp() {
		return dateTimeStamp;
	}

	public void setDateTimeStamp(long dateTimeStamp) {
		this.dateTimeStamp = dateTimeStamp;
	}

	public long getChatRequestTime() {
		return chatRequestTime;
	}

	public void setChatRequestTime(long chatRequestTime) {
		this.chatRequestTime = chatRequestTime;
	}

	public String getIsMissed() {
		return isMissed;
	}

	public void setIsMissed(String isMissed) {
		this.isMissed = isMissed;
	}

	public String getChatRequestType() {
		return chatRequestType;
	}

	public void setChatRequestType(String chatRequestType) {
		this.chatRequestType = chatRequestType;
	}

	public int getChannelType() {
		return channelType;
	}

	public void setChannelType(int channelType) {
		this.channelType = channelType;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getIsTriggered() {
		return isTriggered;
	}

	public void setIsTriggered(String isTriggered) {
		this.isTriggered = isTriggered;
	}

	public String getIsHumanHandover() {
		return isHumanHandover;
	}

	public void setIsHumanHandover(String isHumanHandover) {
		this.isHumanHandover = isHumanHandover;
	}

}

public class VbVisitorServingScript {
	
	public static int BATCH_UPDATE_SIZE = 1000;
	public static long TOTAL_UPDATE = 0;
	private static int THREAD_SIZE = 1000;
	
	public void setBatchSize(int size) {
		BATCH_UPDATE_SIZE = size;
	}
	
	public void alterTableEngine(String tableName, String engineName) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("ALTER TABLE " + tableName + "  engine = '"
					+ engineName + "';");
			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("altered table " + tableName + " engine --> " + engineName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void dropTable(String tableName) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate("drop table IF EXISTS  " + tableName + " ;");
			System.out.println("dropped table " + tableName);

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createTable(String tableName, String sourceTable) {
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();

			stmt.executeUpdate("DROP TABLE  IF EXISTS " + tableName);
			stmt.executeUpdate("Create table " + tableName + " LIKE " + sourceTable + ";");

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("created new table " + tableName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * +-------------------------+---------------+------+-----+---------+----------------+
| Field                   | Type          | Null | Key | Default | Extra          |
+-------------------------+---------------+------+-----+---------+----------------+
| ID                      | bigint(20)    | NO   | PRI | NULL    | auto_increment |
| vsAgentID               | bigint(20)    | YES  | MUL | NULL    |                |
| vsVisitorID             | varchar(20)   | YES  | MUL | NULL    |                |
| vsAccount               | varchar(20)   | YES  | MUL | NULL    |                |
| vsConversationStartTime | decimal(18,0) | NO   |     | NULL    |                |
| vsConversationEndTime   | decimal(18,0) | NO   |     | NULL    |                |
| vsVisitorRID            | bigint(20)    | YES  |     | NULL    |                |
| vsLastMsgID             | bigint(20)    | YES  |     | NULL    |                |
| vsChatEndedBy           | tinyint(1)    | YES  |     | -1      |                |
| vsChatInfoId            | bigint(20)    | YES  | MUL | 0       |                |
| isTransferred           | tinyint(11)   | YES  |     | -1      |                |
+-------------------------+---------------+------+-----+---------+----------------+

	 * */

	public void bulkInsert(String destination, String source, long lastId) {
		
		String query = "INSERT INTO " + destination + "(vsAgentID, vsVisitorID, vsAccount, vsConversationStartTime, "
				+ " vsConversationEndTime, vsVisitorRID, vsLastMsgID, vsChatEndedBy, vsChatInfoId, isTransferred)"
				+" SELECT vsAgentID, vsVisitorID, vsAccount, vsConversationStartTime, vsConversationEndTime, vsVisitorRID, "
				+ " vsLastMsgID, vsChatEndedBy, vsChatInfoId, isTransferred  FROM " + source + " WHERE ID <= " + lastId + ";";

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			
			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query);

			stmt.close();
			
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("Bulk inserted into " + destination + " from " + source);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addAdditionalColumn(String tableName) {
		StringBuilder query = new StringBuilder();
		query.append("ALTER TABLE ").append(tableName).append(" ")
				.append("ADD COLUMN chatStatus int(11) DEFAULT NULL, ")
				.append("ADD COLUMN dateTimeStamp decimal(20,0) NOT NULL DEFAULT \"0\", ")
				.append("ADD COLUMN chatRequestTime decimal(20,0) NOT NULL DEFAULT \"0\", ")
				.append("ADD COLUMN isMissed char(1) CHARACTER SET utf8 NOT NULL DEFAULT \"0\", ")
				.append("ADD COLUMN chatRequestType char(1) CHARACTER SET utf8 NOT NULL DEFAULT \"1\", ")
				.append("ADD COLUMN channelType smallint(6) DEFAULT \"0\", ")
				.append("ADD COLUMN departmentId bigint(20) DEFAULT \"0\", ")
				.append("ADD COLUMN isTriggered char(1) DEFAULT \"0\", ")
				.append("ADD COLUMN isHumanHandover char(1) DEFAULT \"0\", ")
				.append("ADD COLUMN rateType varchar(20) DEFAULT NULL, ")
				.append("ADD COLUMN rateValue varchar(20) DEFAULT NULL ")
				.append(";");

		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(query.toString());

			stmt.close();
			DatabaseManager.getInstance().freeConnection(connection);

			System.out.println("Added additional column");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fillupMissedChatMissingColumn(String tableName, long lastId) {

		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			String query = "SELECT m.ID, m.chatStatus, m.dateTimeStamp, m.chatRequestTime, m.isMissed, "
					+ " m.chatRequestType, m.channelType, m.departmentId, m.isTriggered, m.isHumanHandover " + " FROM "
					+ tableName + " s INNER JOIN vbmissedchats m ON s.vsChatInfoId = m.ID WHERE m.ID <= " + lastId + ";";
			
			System.out.println("Query: " + query);

			PreparedStatement ps = connection.prepareStatement(query);

			ResultSet rs = ps.executeQuery();
			
			System.out.println("Query executed");

			ArrayList<MissedChatMissingColumnDTO> list = new ArrayList<>();

			long count = 0;

			while (rs.next()) {
				MissedChatMissingColumnDTO dto = new MissedChatMissingColumnDTO();
				dto.setId(rs.getLong("ID"));
				dto.setChatStatus(rs.getInt("chatStatus"));
				dto.setDateTimeStamp(rs.getLong("dateTimeStamp"));
				dto.setChatRequestTime(rs.getLong("chatRequestTime"));
				dto.setIsMissed("0");
				dto.setChatRequestType(rs.getString("chatRequestType"));
				dto.setChannelType(rs.getInt("channelType"));
				dto.setDepartmentId(rs.getLong("departmentId"));
				dto.setIsTriggered(rs.getString("isTriggered"));
				dto.setIsHumanHandover(rs.getString("isHumanHandover"));

				list.add(dto);

				count++;

//				if (count % 100000 == 0) {
//					System.out.println("List Size: " + list.size() + " data");
//				}
			}

			ps.close();
			rs.close();

			System.out.println("list Size --> " + list.size());

			query = "UPDATE " + tableName
					+ " SET chatStatus = ?, dateTimeStamp = ?, chatRequestTime = ?, isMissed = ?, chatRequestType = ?, "
					+ " channelType = ?, departmentId = ?, isTriggered = ?, isHumanHandover = ? WHERE vsChatInfoId = ?;";
			
			System.out.println("\nQuery: " + query);

			ps = connection.prepareStatement(query);

			int batchCount = 0;

//			for (MissedChatMissingColumnDTO dto : list) {
//				
//				ps.setInt(1, dto.getChatStatus());
//				ps.setLong(2, dto.getDateTimeStamp());
//				ps.setLong(3, dto.getChatRequestTime());
//				ps.setString(4, dto.getIsMissed());
//				ps.setString(5, dto.getChatRequestType());
//				ps.setInt(6, dto.getChannelType());
//				ps.setLong(7, dto.getDepartmentId());
//				ps.setString(8, dto.getIsTriggered());
//				ps.setString(9, dto.getIsHumanHandover());
//				ps.setLong(10, dto.getId());
//
//				ps.addBatch();
//
//				batchCount++;
//
//				if (batchCount % BATCH_UPDATE_SIZE == 0 || batchCount == list.size()) {
//					System.out.println("updating " + batchCount + " rows... :) ");
//					ps.executeBatch();
//					System.out.println("updated " + batchCount + " rows... :) ");
//				}
//			}
			
			ps.close();
			
			long st = System.currentTimeMillis();
			TOTAL_UPDATE = 0;
			int totalThread = 0;
			long rowUpdate = 0;
			
			ArrayList<MissedChatMissingColumnDTO> tempList = new ArrayList<>();
			
			for(MissedChatMissingColumnDTO dto : list) {
				
				tempList.add(dto);
				rowUpdate++;
				batchCount++;

				if (batchCount % THREAD_SIZE == 0 || batchCount == list.size()) {
					TOTAL_UPDATE++;
					MissingColumnUpdateThread uThread = new MissingColumnUpdateThread(tableName, tempList, batchCount);
					Thread t = new Thread(uThread);
					t.start();
					tempList = new ArrayList<>();
					totalThread++;
					if(totalThread >= 100) {
						System.out.println("Total Thread: " + totalThread);
						while(TOTAL_UPDATE > 0) {
							Thread.sleep(5000);
							System.out.println("Waiting for finish... Target: " + rowUpdate + " Progress: " + TOTAL_UPDATE + " Total: " + batchCount);
							continue;
						}
						TOTAL_UPDATE = 0;
						totalThread = 0;
						rowUpdate = 0;
					}
				}
			}
			
			System.out.println("Total Thread: " + totalThread);
			
			while(TOTAL_UPDATE > 0) {
				
				Thread.sleep(10000);
				System.out.println("Waiting for finish... Target: " + rowUpdate + " Progress: " + TOTAL_UPDATE + " Total: " + batchCount);
				
				continue;
			}
			
			long et = System.currentTimeMillis();
			System.out.println("Time required to update: " + (et - st));

			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("missed chat missing column inserted");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertMissedChatInformation(String tableName, long lastId) {
		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			String query = "SELECT * FROM vbmissedchats WHERE isMissed = 1 AND ID <= " + lastId + ";";
			
			System.out.println("Query: " + query);

			PreparedStatement ps = connection.prepareStatement(query);

			ResultSet rs = ps.executeQuery();
			
			System.out.println("Query executed");

			ArrayList<MissedChatMissingColumnDTO> list = new ArrayList<>();

			long count = 0;

			while (rs.next()) {
				MissedChatMissingColumnDTO dto = new MissedChatMissingColumnDTO();
				dto.setId(rs.getLong("ID"));
				dto.setVbAccount(rs.getString("vbAccount"));
				dto.setAssignedAgentId(rs.getLong("assignedAgentID"));
				dto.setAssignedVisitorId(rs.getLong("assignedVisitorID"));
				dto.setVsRecordId(rs.getLong("vsRecordID"));
				dto.setChatStatus(rs.getInt("chatStatus"));
				dto.setDateTimeStamp(rs.getLong("dateTimeStamp"));
				dto.setChatRequestTime(rs.getLong("chatRequestTime"));
				dto.setChatResponseTime(rs.getLong("chatResponseTime"));
				dto.setIsMissed(rs.getString("isMissed"));
				dto.setChatRequestType(rs.getString("chatRequestType"));
				dto.setIsTransferred(rs.getInt("isTransferred"));
				dto.setChannelType(rs.getInt("channelType"));
				dto.setDepartmentId(rs.getLong("departmentId"));
				dto.setIsTriggered(rs.getString("isTriggered"));
				dto.setIsHumanHandover(rs.getString("isHumanHandover"));

				list.add(dto);

				count++;

//				if (count % 10000 == 0) {
//					System.out.println("inserted " + list.size() + " data");
//				}
			}

			ps.close();
			rs.close();

			System.out.println("list Size --> " + list.size());

			query = "INSERT INTO " + tableName
					+ "(vsAgentID, vsVisitorID, vsAccount, vsConversationStartTime, vsConversationEndTime, vsVisitorRID, vsLastMsgID, vsChatEndedBy, "
					+ " vsChatInfoId, isTransferred, "
					+ " chatStatus, dateTimeStamp, chatRequestTime, isMissed, chatRequestType, channelType, departmentId, isTriggered, isHumanHandover) "
					+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			System.out.println("Query: " + query);

			ps = connection.prepareStatement(query);

			int batchCount = 0;

//			for (MissedChatMissingColumnDTO dto : list) {
//				ps.setLong(1, dto.getAssignedAgentId());
//				ps.setLong(2, dto.getAssignedVisitorId());
//				ps.setString(3, dto.getVbAccount());
//				ps.setLong(4, dto.getChatResponseTime());
//				ps.setLong(5, 0);
//				ps.setLong(6, dto.getVsRecordId());
//				ps.setLong(7, -1);
//				ps.setInt(8, -1);
//				ps.setLong(9, dto.getId());
//				ps.setInt(10, dto.getIsTransferred());
//
//				ps.setInt(11, dto.getChatStatus());
//				ps.setLong(12, dto.getDateTimeStamp());
//				ps.setLong(13, dto.getChatRequestTime());
//				ps.setString(14, dto.getIsMissed());
//				ps.setString(15, dto.getChatRequestType());
//				ps.setInt(16, dto.getChannelType());
//				ps.setLong(17, dto.getDepartmentId());
//				ps.setString(18, dto.getIsTriggered());
//				ps.setString(19, dto.getIsHumanHandover());
//
//				ps.addBatch();
//
//				batchCount++;
//
//				if (batchCount % BATCH_UPDATE_SIZE == 0 || batchCount == list.size()) {
//					ps.executeBatch();
//					System.out.println("inserted " + batchCount + " rows... :) ");
//				}
//			}

			ps.close();
			
			long st = System.currentTimeMillis();
			TOTAL_UPDATE = 0;
			int totalThread = 0;
			
			ArrayList<MissedChatMissingColumnDTO> tempList = new ArrayList<>();
			for (MissedChatMissingColumnDTO dto : list) {
				
				tempList.add(dto);

				batchCount++;

				if (batchCount % THREAD_SIZE == 0 || batchCount == list.size()) {
					TOTAL_UPDATE++;
					MissedChatInfoInsertThread uThread = new MissedChatInfoInsertThread(tableName, tempList, batchCount);
					Thread t = new Thread(uThread);
					t.start();
					tempList = new ArrayList<>();
					totalThread++;
				}
			}
			
			System.out.println("Total Thread: " + totalThread);
			
			while(TOTAL_UPDATE > 0) {
				
				Thread.sleep(20000);
				System.out.println("Waiting for finish... Target: " + list.size() + " Progress: " + TOTAL_UPDATE);
				
				continue;
			}
			
			long et = System.currentTimeMillis();
			System.out.println("Time required to update: " + (et - st));

			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("missed chat data inserted");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertRatingInformation(String tableName, long lastId) {
		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();

			String query = "SELECT visitorServingID, rateType, rateValue FROM vbrating WHERE ID <= " + lastId + ";";

			PreparedStatement ps = connection.prepareStatement(query);

			ResultSet rs = ps.executeQuery();

			ArrayList<RatingDTO> list = new ArrayList<>();

			long count = 0;

			while (rs.next()) {
				RatingDTO dto = new RatingDTO();
				dto.setVisitorServingID(rs.getLong("visitorServingID"));
				dto.setRateType(rs.getString("rateType"));
				dto.setRateValue(rs.getString("rateValue"));

				list.add(dto);

				count++;
			}

			ps.close();
			rs.close();

			System.out.println("list Size --> " + list.size());

			query = "UPDATE " + tableName + " SET rateType = ?, rateValue = ? WHERE ID = ?;";

			ps = connection.prepareStatement(query);

			int batchCount = 0;

//			for (RatingDTO dto : list) {
//				ps.setString(1, dto.getRateType());
//				ps.setString(2, dto.getRateValue());
//				ps.setLong(3, dto.getVisitorServingID());
//
//				ps.addBatch();
//
//				batchCount++;
//
//				if (batchCount > 0 && (batchCount % BATCH_UPDATE_SIZE == 0 || batchCount == list.size())) {
//					System.out.println("updating " + batchCount + " rows... :) ");
//					ps.executeBatch();
//					System.out.println("updated " + batchCount + " rows... :) ");
//				}
//			}

			ps.close();
			
			long st = System.currentTimeMillis();
			
			ArrayList<RatingDTO> tempList = new ArrayList<>();
			TOTAL_UPDATE = 0;
			int totalThread = 0;

			for (RatingDTO dto : list) {
				
				tempList.add(dto);

				batchCount++;

				if (batchCount % THREAD_SIZE == 0 || batchCount == list.size()) {
					TOTAL_UPDATE++;
					RatingInfoUpdateThread uThread = new RatingInfoUpdateThread(tableName, tempList, batchCount);
					Thread t = new Thread(uThread);
					t.start();
					tempList = new ArrayList<>();
					totalThread++;
				}
			}
			
			System.out.println("Total Thread: " + totalThread);
			
			while(TOTAL_UPDATE > 0) {
				
				Thread.sleep(20000);
				System.out.println("Waiting for finish... Target: " + list.size() + " Progress: " + TOTAL_UPDATE);
				long et = System.currentTimeMillis();
				
				continue;
			}

			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("Rating data inserted");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateChatTransferTable(String tableName) {

		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			ResultSet rs = null;
			PreparedStatement ps = null;
			String query = null;
			
			alterTableEngine("vbchattransfer", "MyISAM");

			try {
				query = "ALTER TABLE vbchattransfer DROP COLUMN vbServingRowID;";
				ps = connection.prepareStatement(query);
				ps.executeUpdate();
				ps.close();
				System.out.println("delete vbServingRowID column from vbchattransfer");
			} catch (Exception e) {
				// e.printStackTrace();
				System.out.println("vbServingRowID does not exists");
			}

			query = "ALTER TABLE vbchattransfer ADD COLUMN vbServingRowID bigint(20) DEFAULT NULL;";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			System.out.println("added new column vbServingRowID into vbchattransfer");

			query = "SELECT s.ID servingId, s.vsChatInfoId missedId FROM " + tableName
					+ " s INNER JOIN vbchattransfer t ON s.vsChatInfoId = t.vbmissedRowID";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			ArrayList<MissedIdTOServingIdDTO> list = new ArrayList<>();

			while (rs.next()) {

				MissedIdTOServingIdDTO dto = new MissedIdTOServingIdDTO();
				dto.setServingId(rs.getLong("servingId"));
				dto.setMissedChatId(rs.getLong("missedId"));
				
				list.add(dto);
			}

			System.out.println("list size: " + list.size());

			ps.close();
			rs.close();
			
			

			query = "UPDATE vbchattransfer SET vbServingRowID = ? WHERE vbmissedRowID = ?;";
			ps = connection.prepareStatement(query);

			long batchCount = 0;
			long totalUpdate = 0;

			for (MissedIdTOServingIdDTO dto : list) {
				if(dto.getServingId() > 0 && dto.getMissedChatId() >0) {
					ps.setLong(1, dto.getServingId());
					ps.setLong(2, dto.getMissedChatId());
	
					ps.addBatch();
	
					batchCount++;
				}

				if (batchCount > 0 && batchCount % BATCH_UPDATE_SIZE == 0) {
					ps.executeBatch();
					totalUpdate += batchCount;
					batchCount = 0;
					System.out.println("updated " + totalUpdate + " rows... :) ");
				}
			}
			
			if (batchCount > 0) {
				ps.executeBatch();
				totalUpdate +=batchCount;
				System.out.println("updated " + totalUpdate + " rows... :) ");
			}
			
			alterTableEngine("vbchattransfer", "InnoDB");

			ps.close();
			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("vbchattransfer updated...");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateSuccessfullChatRequestTable(String tableName) {

		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			ResultSet rs = null;
			PreparedStatement ps = null;
			String query = null;
			
			alterTableEngine("vbsuccessfulchatrequest", "MyISAM");

			try {
				query = "ALTER TABLE vbsuccessfulchatrequest DROP COLUMN vbServingRowID;";
				ps = connection.prepareStatement(query);
				ps.executeUpdate();
				ps.close();
				System.out.println("delete vbServingRowID column from vbsuccessfulchatrequest");
			} catch (Exception e) {
				// e.printStackTrace();
				System.out.println("vbServingRowID does not exists");
			}

			query = "ALTER TABLE vbsuccessfulchatrequest ADD COLUMN vbServingRowID bigint(20) DEFAULT NULL;";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			System.out.println("added new column vbServingRowID into vbsuccessfulchatrequest");

			query = "SELECT s.ID servingId, s.vsChatInfoId missedId FROM " + tableName
					+ " s INNER JOIN vbsuccessfulchatrequest t ON s.vsChatInfoId = t.chat_info_id";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			ArrayList<MissedIdTOServingIdDTO> list = new ArrayList<>();

			while (rs.next()) {

				MissedIdTOServingIdDTO dto = new MissedIdTOServingIdDTO();
				dto.setServingId(rs.getLong("servingId"));
				dto.setMissedChatId(rs.getLong("missedId"));
				
				list.add(dto);
			}

			System.out.println("list size: " + list.size());

			ps.close();
			rs.close();
			
			

			query = "UPDATE vbsuccessfulchatrequest SET vbServingRowID = ? WHERE chat_info_id = ?;";
			ps = connection.prepareStatement(query);

			long batchCount = 0;
			long totalUpdate = 0;

			for (MissedIdTOServingIdDTO dto : list) {
				
				if(dto.getServingId() > 0 && dto.getMissedChatId() >0) {
					ps.setLong(1, dto.getServingId());
					ps.setLong(2, dto.getMissedChatId());
	
					ps.addBatch();
	
					batchCount++;
				}

				if (batchCount > 0 && batchCount % BATCH_UPDATE_SIZE == 0){
					ps.executeBatch();
					totalUpdate +=batchCount;
					batchCount = 0;
					System.out.println("updated " + totalUpdate + " rows... :) ");
				}
			}
			
			if (batchCount > 0){
				ps.executeBatch();
				totalUpdate +=batchCount;
				System.out.println("updated " + totalUpdate + " rows... :) ");
			}
			
			alterTableEngine("vbsuccessfulchatrequest", "InnoDB");
			
			ps.close();

			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("vbsuccessfulchatrequest updated...");
			
			query = "create index Idx_vbServingRowID on vbsuccessfulchatrequest(vbServingRowID);";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			System.out.println("additional index added into vbsuccessfulchatrequest");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateStoreTagInfoTable(String tableName) {

		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			ResultSet rs = null;
			PreparedStatement ps = null;
			String query = null;
			
			alterTableEngine("vbstoretaginfo", "MyISAM");

			try {
				query = "ALTER TABLE vbstoretaginfo DROP COLUMN vbServingRowID;";
				ps = connection.prepareStatement(query);
				ps.executeUpdate();
				ps.close();
				System.out.println("delete vbServingRowID column from vbstoretaginfo");
			} catch (Exception e) {
				// e.printStackTrace();
				System.out.println("vbServingRowID does not exists");
			}

			query = "ALTER TABLE vbstoretaginfo ADD COLUMN vbServingRowID bigint(20) DEFAULT NULL;";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			System.out.println("added new column vbServingRowID into vbstoretaginfo");

			query = "SELECT s.ID servingId, s.vsChatInfoId missedId FROM " + tableName
					+ " s INNER JOIN vbstoretaginfo t ON s.vsChatInfoId = t.chatInfoId";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			ArrayList<MissedIdTOServingIdDTO> list = new ArrayList<>();

			while (rs.next()) {

				MissedIdTOServingIdDTO dto = new MissedIdTOServingIdDTO();
				dto.setServingId(rs.getLong("servingId"));
				dto.setMissedChatId(rs.getLong("missedId"));
				
				list.add(dto);
			}

			System.out.println("list size: " + list.size());

			ps.close();
			rs.close();
			
			

			query = "UPDATE vbstoretaginfo SET vbServingRowID = ? WHERE chatInfoId = ?;";
			ps = connection.prepareStatement(query);

			long batchCount = 0;
			long totalUpdate = 0;

			for (MissedIdTOServingIdDTO dto : list) {
				
				if(dto.getServingId() > 0 && dto.getMissedChatId() >0) {
					ps.setLong(1, dto.getServingId());
					ps.setLong(2, dto.getMissedChatId());
	
					ps.addBatch();
	
					batchCount++;
				}

				if (batchCount > 0 && batchCount % BATCH_UPDATE_SIZE == 0){
					ps.executeBatch();
					totalUpdate +=batchCount;
					batchCount = 0;
					System.out.println("updated " + totalUpdate + " rows... :) ");
				}
			}
			
			if (batchCount > 0){
				ps.executeBatch();
				totalUpdate +=batchCount;
				System.out.println("updated " + totalUpdate + " rows... :) ");
			}
			
			alterTableEngine("vbstoretaginfo", "InnoDB");
			
			ps.close();

			DatabaseManager.getInstance().freeConnection(connection);
			
			list = null;

			System.out.println("vbstoretaginfo updated...");
			
			query = "create index Idx_vbServingRowID_storetaginfo on vbstoretaginfo(vbServingRowID);";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			System.out.println("additional index added into vbstoretaginfo");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addAditionalIndexForServingTable(String tableName) {
		try {

			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			ResultSet rs = null;
			PreparedStatement ps = null;
			String query = null;
			
			query = "create index Idx_account_isMissed_chatReqType_chatReqTime_serving on " + tableName + "(vsAccount, isMissed, chatRequestType, chatRequestTime);";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			
			query = "create index Idx_vbAccount_chatStatus_dateTimeStamp_serving on " + tableName + "(vsAccount, chatStatus, dateTimeStamp);";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			
			query = "create index Idx_vsVisitorRID_dateTimeStamp_serving on " + tableName + "(vsVisitorRID, dateTimeStamp);";
			ps = connection.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
			
//			query = "create index idx_vsChatInfoId on " + tableName + "(vsChatInfoId);";
//			ps = connection.prepareStatement(query);
//			ps.executeUpdate();
//			ps.close();

			System.out.println("additional index added into " + tableName);
			
			DatabaseManager.getInstance().freeConnection(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public long lastID(String tableName) {
		long id = -1;
		
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			Connection connection = databaseManager.getConnection();
			PreparedStatement ps;
			ResultSet rs = null;
			
			String query = "SELECT ID FROM " + tableName + " ORDER BY ID DESC LIMIT 1";
			ps = connection.prepareStatement(query);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				id = rs.getLong("ID");
			}
			
			ps.close();
			rs.close();
			
			DatabaseManager.getInstance().freeConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
	}

}
