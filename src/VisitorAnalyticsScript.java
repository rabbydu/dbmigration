/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import databasemanager.DatabaseManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.JDOMException;

/**
 *
 * @author subrata.pappu
 */
class UserAccount {

    String operatorCode;
    long trackingID;
}

class VisitorAnalyticsDTO {

    long ms;
    int pageViews;
    int pageVisits;
    int uniqueVisitors;

    public long getMs() {
        return ms;
    }

    public void setMs(long ms) {
        this.ms = ms;
    }

    public int getPageViews() {
        return pageViews;
    }

    public void setPageViews(int pageViews) {
        this.pageViews = pageViews;
    }

    public int getPageVisits() {
        return pageVisits;
    }

    public void setPageVisits(int pageVisits) {
        this.pageVisits = pageVisits;
    }

    public int getUniqueVisitors() {
        return uniqueVisitors;
    }

    public void setUniqueVisitors(int uniqueVisitors) {
        this.uniqueVisitors = uniqueVisitors;
    }
}
// populate vbVisitorAnalytics table with existing data
public class VisitorAnalyticsScript {

    static final int BATCH_SIZE = 5000;

    static Map<String, VisitorAnalyticsDTO> map = new HashMap<String, VisitorAnalyticsDTO>();
    static List<UserAccount> listUserAccounts = new ArrayList<UserAccount>();

    public static void populateVbVisitorAnalytics() {
        Connection connection = null;
        ResultSet rsAllOpcodes = null;
        Statement stAllOpcodes = null;
        String getAllOpcodesQuery = "SELECT clAccount,clTrackingID FROM vbclient;";
        String getDataFromvbwebsitedailysummaryTableQuery = ""
                + "SELECT * FROM "
                + "vbwebsitedailysummary "
                + "WHERE analytics_id=?";
        String getUniqueVisitorQuery = "SELECT "
                + "vsFirstVisitTime, "
                + "COUNT(*) total "
                + "FROM vbvisitor WHERE vsAccount = ? "
                + "GROUP BY DATE_FORMAT( FROM_UNIXTIME((vsFirstVisitTime) / 1000), \"%Y-%m-%d\" )";
        String insertIntovbVisitorAnalyticsQuery = "INSERT INTO"
                + " vbVisitorAnalytics(vbAccount,vbdate,page_views,page_visits,unique_visitors) VALUES(?,?,?,?,?)";
        PreparedStatement psVbWebsiteDailySummary = null;
        PreparedStatement psVbVisitor = null;
        PreparedStatement psVbVisitorAnalytics = null;
        ResultSet rsVbWebsiteDailySummary = null;
        ResultSet rsVbVisitor = null;
        int notFound = 0;
        try {
            connection = DatabaseManager.getInstance().getConnection();
            System.out.println("Got Connection");
            stAllOpcodes = connection.createStatement();
            rsAllOpcodes = stAllOpcodes.executeQuery(getAllOpcodesQuery);
            psVbWebsiteDailySummary = connection.prepareStatement(getDataFromvbwebsitedailysummaryTableQuery);
            psVbVisitor = connection.prepareStatement(getUniqueVisitorQuery);
            psVbVisitorAnalytics = connection.prepareStatement(insertIntovbVisitorAnalyticsQuery);

            while (rsAllOpcodes.next()) {
                try {
                    UserAccount userAccount = new UserAccount();
                    userAccount.operatorCode = rsAllOpcodes.getString("clAccount");
                    userAccount.trackingID = rsAllOpcodes.getLong("clTrackingID");
                    listUserAccounts.add(userAccount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            for (UserAccount userAccount : listUserAccounts) {
                try {
                    String operatorCode = userAccount.operatorCode;
                    long trackingID = userAccount.trackingID;
                    psVbWebsiteDailySummary.setLong(1, trackingID);
                    rsVbWebsiteDailySummary = psVbWebsiteDailySummary.executeQuery();

                    int n = 0;

                    //System.out.println("PROCESSING DATA OF opcdoe "+operatorCode);
                    while (rsVbWebsiteDailySummary.next()) {
                        try {
                            VisitorAnalyticsDTO dto = new VisitorAnalyticsDTO();
                            dto.setMs(rsVbWebsiteDailySummary.getLong("vbdate"));
                            dto.setPageViews(rsVbWebsiteDailySummary.getInt("pageviews"));
                            dto.setPageVisits(rsVbWebsiteDailySummary.getInt("pagevisits"));

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(dto.getMs());

                            String key = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                            map.put(key, dto);
                            n++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                   //System.out.println(n + " records");

                    psVbVisitor.setString(1, operatorCode);
                    rsVbVisitor = psVbVisitor.executeQuery();

                    while (rsVbVisitor.next()) {
                        long firstVisitTime = rsVbVisitor.getLong("vsFirstVisitTime");
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(firstVisitTime);

                        String key = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        VisitorAnalyticsDTO dto = map.get(key);
                        if (dto != null) {
                            dto.setUniqueVisitors(rsVbVisitor.getInt("total"));
                            map.put(key, dto);
                        } else {
                            //System.out.println(key);
                            notFound++;
                        }
                    }

                    int count = 1;
                    for (Map.Entry<String, VisitorAnalyticsDTO> entry : map.entrySet()) {
                        try {
                            VisitorAnalyticsDTO dto = entry.getValue();
                            psVbVisitorAnalytics.setString(1, operatorCode);
                            psVbVisitorAnalytics.setLong(2, dto.getMs());
                            psVbVisitorAnalytics.setInt(3, dto.getPageViews());
                            psVbVisitorAnalytics.setInt(4, dto.getPageVisits());
                            psVbVisitorAnalytics.setInt(5, dto.getUniqueVisitors());
                            psVbVisitorAnalytics.addBatch();
                            count++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (psVbVisitorAnalytics != null) {
                        psVbVisitorAnalytics.executeBatch();
                        map.clear();
                    }
                    //System.out.println(notFound + " records not found");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (JDOMException ex) {
            Logger.getLogger(VisitorAnalyticsScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(VisitorAnalyticsScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VisitorAnalyticsScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(VisitorAnalyticsScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(VisitorAnalyticsScript.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
}
